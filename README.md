# WebMallet

Web interface for MALLET library.
More information about MALLET (MAchine Learning for LanguagE Toolkit) can be found at http://mallet.cs.umass.edu/

Interfaccia Web per la libreria MALLET.
Per ulteriori informazioni su MALLET (MAchine Learning for LanguagE Toolkit) vedere http://mallet.cs.umass.edu/