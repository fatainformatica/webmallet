package it.fatainformatica.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import cc.mallet.types.InstanceList;
import it.fatainformatica.Import;

/**
 *  this Servlet receive parameters from import.jsp and gives them to Import class
 *  then let the user download the file
 */

@SuppressWarnings("serial")
@WebServlet("/import")

public class ImportServlet extends HttpServlet {

	private File file;
	private int maxMemSize = 10000 * 1024;
	private int maxFileSize = 10000 * 1024;
	private String filePath;
	private String name = null;
	
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		//Verify content type
		String contentType = req.getContentType();
		
		filePath = getServletContext().getRealPath("") + File.separator + "ImportFile";
		
		/*
		 * check if parameters sent from import.jsp exist.
		 * If not forward error message to import.jsp
		 */
		
		if ((contentType.indexOf("multipart/form-data") >= 0)) {
			
			DiskFileItemFactory factory = new DiskFileItemFactory();
			
			//maximum size that will be stored in memory
			factory.setSizeThreshold(maxMemSize);
			
			//location to save data that is larger than maxMemSize
			factory.setRepository(new File("C:\\temp"));
			
			//create new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);
			
			//maximum file size to be uploaded
			upload.setSizeMax(maxFileSize);
			
			try {
				//parse req to get fileItems and gives them to
				//upload method
				
				List<FileItem> fileItems = upload.parseRequest(req);
				
				//process the uploaded file items
				Iterator<FileItem> it = fileItems.iterator();
				
				while (it.hasNext()) {
					
					FileItem fi = (FileItem) it.next();
					
					// if fileItem is not form field save the file, else if form field equals
					// OutputName it is assigned to String name
					if ( !fi.isFormField() ) {
						
						// Write the File
						file = new File( filePath + "\\uploadedFile.txt");
						fi.write( file );
					}
					else {
						
						String fieldName = fi.getFieldName();
						String fieldValue = fi.getString();
						
						// assignment to name 
						if (fieldName.equals("OutputName"))
							name = fieldValue;
					}
				}
			} catch (Exception ex) {
				//TODO crea file di log
				ex.printStackTrace();
			}
		}

		else 
	        req.setAttribute("msgError", "Error! Please insert a file to be uploaded");
		
		if ( name != null ) {
			
			Import importer = new Import();
			InstanceList instances = importer.readFile(new File(filePath + "\\uploadedFile.txt"));
			instances.save(new File(filePath + File.separator + name + "_import.mallet"));
			downloadFile(resp);
			req.setAttribute("msgSuccess", "Import Succesful!");	
		}
		
		else 
	        req.setAttribute("msgError", "Error! Please insert File Name");
        
        req.getRequestDispatcher("import.jsp").forward(req, resp);
	}
	
	//let the user download the file with inserted name
	private void downloadFile(HttpServletResponse resp) {
		
		resp.setContentType("APPLICATION/OCTET-STREAM");
		resp.setHeader("Content-Disposition", "attachment;filename=\"" + name + "_import.mallet\"");
		
		try (FileInputStream fileinputStream = new FileInputStream(filePath + File.separator + name + "_import.mallet")) {
			
			ServletOutputStream out = resp.getOutputStream();
			
			byte[] outputByte = new byte[4096];
			//copy binary contect to output stream
			while(fileinputStream.read(outputByte, 0, 4096) != -1)
			{
				out.write(outputByte, 0, 4096);
			}
			
			out.flush();
			out.close();
			
		} catch (IOException ex) {
			// TODO create log file
			ex.printStackTrace();
		}
	}
	
	
}
