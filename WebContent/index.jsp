<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Mallet Web Interface</title>
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		
	</head>
	<body>
		<div class="container">
			<div style="text-align: center">
			<h1>Welcome to Mallet Web Application</h1>
			<h4>MAchine Learning for LanguagE Toolkit</h4>
			<div class="dropdown"  style="margin-top: 30px">
  				<a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    				What do you want to do?
  				</a>
  			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    			<a class="dropdown-item" href="import.jsp">1 - Import </a>
    			<a class="dropdown-item" href="training.jsp">2 - Training</a>
    			<a class="dropdown-item" href="classification.jsp">3 - Classification</a>
  			</div>
			</div>
		</div>
		</div>
		
		<footer style="text-align: right">Fata Intormatica &copy;</footer>
		
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</body>
</html>